/*
 * domolib.c
 *
 * Copyright 2018 Daniel Jenkins <jeukel@Galago>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
#include "../include/domolib.h"

const int pinsLights[] = {2,3,4,14,15};
const int pinsDoors[] = {18,17,27,22};

/*
* Setup variables
*/
void init(){
  char in = 'i';
  char out = 'o';
  int c;

  /*
  * Ligths Setup
  */
  //TODO

  /*
  * Sensors Setup
  */
  //TODO

  printf("Initializing...\n");
  printf("STARTING LIGHTS...\n");
  for(c=0; c<LIGHTS ;c++)
  {
    //printf("El [] requerido es: %d\n",c );
    //printf("El pin equivalente es: %d\n",pinsDoors[c] );
    pinMode(pinsLights[c],out);
    sleep(1);
  }
  printf("LIGHTS SET\n");

  printf("STARTING DOORS...\n");
  for(c=0; c<DOORS ;c++)
  {
    //printf("El [] requerido es: %d\n",c );
    //printf("El pin equivalente es: %d\n",pinsDoors[c] );
    pinMode(pinsDoors[c],in);
    sleep(1);
  }
  printf("DOORS SET\n");
}

/*
 * light(name, state) [Set a pin into it's state]
 */
int light(int pin, int value){
  int pinIO = pinsLights[pin-1];
  printf("SETTING LIGHT %d (pin #%d) to %d\n",pin,pinIO,value );
  int state = digitalWrite(pinIO, value);
  return state;
}

/*
 * Extra method for multiple lights
 */
int multipleLights(char valueC){
  int tmp = 0;
  int state = 0;  //no errors by default
  int value;

  if(valueC == '0')
  {
    value = 0;
  }else{
    value = 1;
  }

  for(size_t c = 1; c < LIGHTS+1; c++)
  {
    tmp = light(c, value);
    if(tmp != 0){state = -1;}
  }

  return state;
}

/*
 * lightsOn [Turn  on all lights]
 */
int lightsOn(){
  const char value = '1'; //
  int ret_val = multipleLights(value);
  printf("ON ALL LIGHTS\n");
  return ret_val;
}

/*
 * LightsOff [Turn  off all lights]
 */
int lightsOff(){
  const char value = '0'; //
  int ret_val = multipleLights(value);
  printf("OFF ALL LIGHTS\n");
  return ret_val;
}

/*
 * monDoor(pin)
 */
int* monLights(){
  static int state[LIGHTS];
  int n = sizeof(state)/sizeof(state[0]);
  for(int e = 0; e<n ; e++)
  {
    state[e] = digitalRead(pinsLights[e]);
    printf( "LIGHT #%d: %d\n", e, state[e]);
  }
  return state;
}

/* monDoor(pin) */
int* monDoors()
{
  static int state[DOORS];
  int n = sizeof(state)/sizeof(state[0]);
  for(int e = 0; e<n ; e++)
  {
    state[e] = digitalRead(pinsDoors[e]);
    printf( "DOOR #%d: %d\n", e, state[e]);
  }
  return state;
}

/*
 * monDoor(pin)
 */
int monLight(int e)
{
  int state;
  state = digitalRead(pinsLights[e]);
  printf( "LIGHT #%d: %d\n", e, state);
  return state;
}

/* monDoor(pin) */
int monDoor(int e)
{
  int state;
  state = digitalRead(pinsDoors[e]);
  printf( "DOOR #%d: %d\n", e, state);
  return state;
}
