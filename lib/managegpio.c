#include "../include/managegpio.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

/*
* Global variables
*/
FILE *fp;
char gpioPath[] = "/sys/class/gpio/gpio";
char pinDir[] = "/direction";
char pinVal[] = "/value";

/*
* Generates path to specific pinIO
*/
char* concat(char* pin){
  char* midPath;
  /* make space for the new string (should check the return value ...) */
  midPath = malloc(strlen(gpioPath)+strlen(pin)+1);
  strcpy(midPath, gpioPath); /* copy name into the new var */
  strcat(midPath, pin);
  return midPath;
}

/*
* Sets path for values or direction.
*/
char* setPath(int pinIO, int opt){
  char* midPath;

  char str[32];
  sprintf(str, "%d", pinIO);

  char* pin = str;
  midPath = concat(pin);

  char* fullPath;
  /*
  * opt 1 for mode, 2 for value
  */
  switch (opt) {
    /* To set direction */
    case 1:
    {
      fullPath = malloc(strlen(midPath)+strlen(pinDir)+1);
      strcpy (fullPath,midPath);
      strcat (fullPath,pinDir);
      break;
    }
    /* To set value */
    case 2:
    {
      fullPath = malloc(strlen(midPath)+strlen(pinVal)+1);
      strcpy (fullPath,midPath);
      strcat (fullPath,pinVal);
      break;
    }
  }
  return fullPath;
}

/*
 * Unexport pin
 */
int unexport(char pin, char MODE){
  // First export pin
  const char blockPin[] = "/sys/class/gpio/unexport";

  fp = fopen( blockPin, "w" );
  if(fp == NULL){
    printf("file export not found.\n");
    return -1;
  }

  char pinstr[32];
  sprintf(pinstr, "%d", pin);

  const char* pinData = pinstr;

  fputs(pinData, fp); //writes pin on use
  fclose(fp);
  return 0;
}

/*
* Permite establecer entrada el (modo/salida) de un pin especı́fico (número de
* pin/gpio).
*/
int pinMode(char pin, char MODE){
  // First export pin
  const char blockPin[] = "/sys/class/gpio/export";

  fp = fopen( blockPin, "w" );
  if(fp == NULL){
    printf("file export not found.\n");
    return -1;
  }

  char pinstr[32];
  sprintf(pinstr, "%d", pin);

  const char* pinData = pinstr;

  fputs(pinData, fp); //writes pin on use
  fclose(fp);

  // Then to set mode
  char* fullPath = setPath(pin,1);

  fp = fopen( fullPath, "w" );
  if(fp==NULL){
    printf("file setmode not found.\n");
    return -1;
  }

  char md[] = "out";
  char in[] = "in";
  if(MODE == 'i'){
    strcpy(md,in);
  }
  if(fp!=NULL){
    fputs(md, fp); //writes mode
    fclose(fp);
    return 0;
  }
}

/*
* Permite escribir un valor de 0 o 1 en el pin especı́fico configurado como
* salida.
*/
int digitalWrite(char pin, int value){
  int aval = 0;
  if(aval == 0) // 0 for output
  {
    char* fullPath = setPath(pin,2);
    fp = fopen( fullPath,"w" );

    char pinstr[32];
    sprintf(pinstr, "%d", value);

    const char* val = pinstr;
    if(fp!=NULL){
      fputs(val, fp);
      fclose(fp);
      return 0;
    }else{
      return 1;
    }
  }
  return -1;
}

/*
 * Permite leer el estado (0,1) de un pin digital.
 */
int digitalRead(char pin){
 int state = 0;

 char* fullPath = setPath(pin,2);
 fp = fopen( fullPath,"r" );

 if(fp!=NULL){
   fscanf(fp, "%d", &state);
 }
 fclose(fp);

 return state;
}

/*
* Permite generar un blink (establecer y desestablecer un valor binario)
* en un pin a una frecuencia determinada, por un tiempo de duración
* determinado.
*/ /*
int blink(char pin, int freq, int duration)
{
  char mode = '1';
  size_t dur = 0; // acumulado de tiempo
  char lset = '1'; // led state
  char zero = '0';
  char one = '1';

  // 0. frecuencia = encendido por segundo
  int manyCalls = 2 * freq;
  int slp = 1000/manyCalls;

  if(duration < (slp/1000))
  {
    return -1;
  }

  // 1. get path
  char* fullPath;
  fullPath = setPath(pin, 2);

  // 2. duración = ms
  while(dur <= duration)
  {
    digitalWrite(pin,&lset,1);
    if(lset == one){
      strcpy(&lset,&zero);
    }else{
      strcpy(&lset,&one);
    }
    dur += slp;
    sleep(slp);
  }
  return 0;
}
*/

/*
* Checks for in/out to write.
*/ /*
int checkAval(char pinIO)
{
  char buff[1];
  char* pin = pinIO;
  char* gpio_dir = setPath(pin,1);

  fp = fopen( gpio_dir, "r" );
  if(fp != NULL){
    fscanf(fp, "%c", buff);
  }
  fclose(fp);

  // 0 for output, 1 for input
  if(buff[1] == 'i')
  {
    return 1;
  }

  if(buff[1] == 'o')
  {
    return 0;
  }
  return -1;
}
*/
