#include "include/domolib.h"
#include "include/managegpio.h"

void show(int state)
{
  switch (state) {
    case -1: {printf("Algo raro pasó.\n");break;}
    case 0: {printf("Todo bien.\n");break;}
    case 1: {printf("Se jodió esta mi...\n");break;}
  }
}

void testinit()
{
  char in = 'i';
  char out = 'o';
  int c;

  for(c=0; c<LIGHTS ;c++)
  {
    pinMode(pinsLights[c],out);
  }

  for(c=0; c<DOORS ;c++)
  {
    printf("El [] requerido es: %d\n",c );
    printf("El pin equivalente es: %d\n",pinsDoors[c] );
    pinMode(pinsDoors[c],in);
  }
}

void testwrite()
{
  int value = 1;
  int c;
  for(c=0; c<LIGHTS ;c++)
  {
    digitalWrite(pinsLights[c], value);
    sleep(1);
  }

  value = 0;
  for(c=0; c<LIGHTS ;c++)
  {
    digitalWrite(pinsLights[c], value);
    sleep(1);
  }
}

void testlight()
{
  int state;
  int c;
  for(c=1; c<6; c++)
  {
    state = light(c,1);
    show(state);

    sleep(1);

    state = light(c,0);
    show(state);
  }

  sleep(1);
  state = light(2,1);
  show(state);
  sleep(1);
  state = light(4,1);
  show(state);
  sleep(1);
  state = light(5,1);
  show(state);
  sleep(1);
}

void testAllLights()
{
  lightsOff();
  sleep(1);
  lightsOn();
  sleep(1);
  lightsOff();
  sleep(1);
  testlight();
}

void testmon()
{
  int* p = monLights();

  int i;

  for (i=0 ; i<LIGHTS ; i++ ) {
      printf("Estado de la led %d es: %d\n",  i+1, *(p + i) );
  }

  p = monDoors();

  for (i=0 ; i<DOORS ; i++ ) {
      printf("Estado de la puerta %d es: %d\n",  i+1, *(p + i) );
  }

}
