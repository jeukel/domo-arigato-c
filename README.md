# DOMO ARIGATO: GPIO library usage

```
Hardware: Raspberry Pi 3 model B v1.2
Chip: BCM2837RIFBG

```

## API usage

### Methods:

  1. **lightsOn()**
      - _Summary_:
        - Set all lights to ON.
      - _Params_:
        - int[] gpio: All used arrays.
        - int siz: Explicit size of array.
      - _Return_:
        - int: error, warning, success.

  2. **lightsOff()**
    - _Summary_:
      - Set all lights to OFF.
    - _Params_:
      - int[] gpio: All used arrays.
      - int siz: Explicit size of array.
    - _Return_:
      - int: error, warning, success.

  3. **light()**
    - _Summary_:
      - Set one lights to ON or OFF.
    - _Params_:
      - int pin: Number of pin to access.
      - int value: Value to set GPIO pin.
    - _Return_:
      - int: error, warning, success.

  4. **monDoor()**
    - _Summary_:
      - Return the value of a desired gpio.
    - _Params_:
      - int gpio: gpio to monitorize.
    - _Return_:
      - int: 1 = "open", 0 = "close".

  5. **showVideo()**
    - _Summary_:
      - Initialize video in RPi.
    - _Params_:
      - None.
    - _Return_:
      - int: void*.

  6. **init()**
    - _Summary_:
      - Setup all variables.
    - _Params_:
      - None: Reads from file GPIOSetup.
    - _Return_:
      - int: error, warning, success.

## VARIABLES:

  - **lights:**
    1. Living Room
    2. Kitchen
    3. Dinner Room
    4. Master bedroom
    5. Bathroom

  - **door:**
    1. Front door
    2. Back door
    3. Master bedroom
    4. Bathroom

## TODO:
  1. Save variables to caché on system reboot.
  2. Manage timeline (threads).
  3. Use less memory.
  4. Dinamicly allocate pins to be used (less than max).
  5. Generate macros.
  6. Validate is a output pin to write.
