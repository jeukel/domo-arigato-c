/*
 * GPIO.c
 *
 * Copyright 2018 Daniel Jenkins <jeukel@Galago>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "include/domolib.h"

void show(int state){
  switch (state) {
    case -1: {printf("Algo raro pasó.\n");break;}
    case 0: {printf("Todo bien.\n");break;}
    case 1: {printf("Se jodió esta mi...\n");break;}
  }
}

void test1(){

  init();
  int state;
  int c;

  for(c=0; c<4; c++){
    state = lightsOn();
    show(state);
    sleep(1);

    state = lightsOff();
    show(state);
    sleep(1);
  }

  printf("END LOOP ALL\n");

  for(c=1; c<6; c++)
  {
    state = light(c,1);
    show(state);
    sleep(1);

    state = light(c,0);
    show(state);
    sleep(1);
  }

  printf("END LOOP SOLITARY\n");

  state = light(2,1);
  show(state);
  sleep(1);
  state = light(4,1);
  show(state);
  sleep(1);
  state = light(5,1);
  show(state);

  printf("END SET 'RANDOM'\n");

  int *p;

  p = monLights();
  for ( c = 0; c < LIGHTS; c++ ) {
    printf( "LIGHT #%d: %d\n", c, *(p + c));}

  p =  monDoors();
  for ( c = 0; c < DOORS; c++ ) {
    printf( "DOOR #%d: %d\n", c, *(p + c));}

  printf("END PRINT ALL VARS'\n");

  int *u;
  u = &c;

  for(c=0 ; c<6 ; c++)
  {
    monLight(c);
  }

}

int main(int argc, char **argv)
{
  test1();
	return 0;
}
