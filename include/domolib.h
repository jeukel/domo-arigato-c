#ifndef DOMOLIB_H
#define DOMOLIB_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "managegpio.h"
#include "errors.h"
#include "setup.h"

void init();
int light(int pin, int value);
int multipleLights(char value);
int lightsOn();
int lightsOff();
int* monDoors();
int* monLights();
int monDoor(int e);
int monLight(int e);

#endif // DOMOLIB_H
