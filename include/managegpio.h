#ifndef MANAGEGPIO_H
#define MANAGEGPIO_H

//int checkAval(char pinIO);
char* concat(char* pin);
char* setPath(int pinIO, int opt);
int pinMode (char pin, char MODE);
int unexport (char pin, char MODE);
int digitalWrite(char pin, int value);
int digitalRead(char pin);
//int blink(char pin, int freq, int duration);

#endif // MANAGEGPIO_H
