#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  getVideo.py
#
#  Copyright 2018 Daniel Jenkins <jeukel@Galago>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import pygame, sys
import pygame.camera
from pygame.locals import *

def main(args):
    pygame.init()
    pygame.camera.init()

    screen = pygame.display.set_mode((1280,720))

    cam = pygame.camera.Camera("/dev/video0", (1280,720))
    cam.start()

    while 1:
        image = cam.get_image()
        screen.blit(image, (0,0))
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
    #return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))



