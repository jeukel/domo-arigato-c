/*
 * GPIO.c
 *
 * Copyright 2018 Daniel Jenkins <jeukel@Galago>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <stdio.h>
#include <unistd.h>
//#include "include/domolib.h"
//#include "include/setup.h"
#include <string.h>
#include <stdlib.h>
#include "include/setup.h"
#include "include/errors.h"

/*
* Global variables
*/
FILE *fp;
char gpioPath[] = "/sys/class/gpio/gpio";
char pinDir[] = "/direction";
char pinVal[] = "/value";

const int pinsLights[] = {2,3,4,14,15};
const int pinsDoors[] = {18,17,27,22};

/*
* Generates path to specific pinIO
*/
char* concat(char* pin)
{
  char* midPath;
  /* make space for the new string (should check the return value ...) */
  midPath = malloc(strlen(gpioPath)+strlen(pin)+1);
  strcpy(midPath, gpioPath); /* copy name into the new var */
  strcat(midPath, pin);
  return midPath;
}

/*
* Sets path for values or direction.
*/
char* setPath(int pinIO, int opt)
{
  char* midPath;

  char str[32];
  sprintf(str, "%d", pinIO);

  char* pin = str;
  midPath = concat(pin);

  char* fullPath;
  /*
  * opt 1 for mode, 2 for value
  */
  switch (opt) {
    /* To set direction */
    case 1:
    {
      fullPath = malloc(strlen(midPath)+strlen(pinDir)+1);
      strcpy (fullPath,midPath);
      strcat (fullPath,pinDir);
      break;
    }
    /* To set value */
    case 2:
    {
      fullPath = malloc(strlen(midPath)+strlen(pinVal)+1);
      strcpy (fullPath,midPath);
      strcat (fullPath,pinVal);
      break;
    }
  }
  return fullPath;
}

int pinMode(int pin, char MODE)
{
  int aval = -1;

  if(aval == -1) // -1 == if available
  {
    // First export pin
    const char blockPin[] = "/sys/class/gpio/export";

    fp = fopen( blockPin, "a+" );
    if(fp == NULL){
      printf("file export not found.\n");
      return -1;
    }

    char pinstr[32];
    sprintf(pinstr, "%d", pin);

    const char* pinData = pinstr;

    fputs(pinData, fp); //writes pin on use
    fclose(fp);

    // Then to set mode
    char* fullPath = setPath(pin,1);

    fp = fopen( fullPath, "w" );
    if(fp==NULL){
      printf("file setmode not found.\n");
      return -1;
    }

    char md[] = "out";
    char in[] = "in";
    if(MODE == '1'){
      strcpy(md,in);
    }
    if(fp!=NULL){
      fputs(md, fp); //writes mode
      fclose(fp);
      return 0;
    }
  }
  printf("Pin already taken. Try with changePinMode(char* pin, char* MODE)\n");
  return 1;
}

int digitalWrite(char pin, int value)
{
  int aval = 0;
  if(aval == 0) // 0 for output
  {
    char* fullPath = setPath(pin,2);
    fp = fopen( fullPath,"w" );

    char pinstr[32];
    sprintf(pinstr, "%d", value);

    const char* val = pinstr;
    if(fp!=NULL){
      fputs(val, fp);
      fclose(fp);
      return 0;
    }else{
      return 1;
    }
  }
  return -1;
}

void show(int state)
{
  switch (state) {
    case -1: {printf("Algo raro pasó.\n");break;}
    case 0: {printf("Todo bien.\n");break;}
    case 1: {printf("Se jodió esta mi...\n");break;}
  }
}

void testinit()
{
  char in = 'i';
  char out = 'o';
  int c;

  for(c=0; c<LIGHTS ;c++)
  {
    pinMode(pinsLights[c],out);
  }

  for(c=0; c<DOORS ;c++)
  {
    printf("El [] requerido es: %d\n",c );
    printf("El pin equivalente es: %d\n",pinsDoors[c] );
    pinMode(pinsDoors[c],in);
  }
}

void testwrite()
{
  int value = 1;
  int c;
  for(c=0; c<LIGHTS ;c++)
  {
    digitalWrite(pinsLights[c], value);
    sleep(1);
  }

  value = 0;
  for(c=0; c<LIGHTS ;c++)
  {
    digitalWrite(pinsLights[c], value);
    sleep(1);
  }
}

int light(int pin, int value)
{
  int pinIO = pinsLights[pin-1];
  printf("El [] requerido es: %d\n",pin );
  printf("El pin que le corresponde es: %d\n",pinIO );
  int state = digitalWrite(pinIO, value);

  if(state == 0)
  {
    return success;
  }

  if(state == 1)
  {
    return error;
  }
  return warning;
}

void testlight()
{
  int state;
  int c;
  for(c=1; c<6; c++)
  {
    state = light(c,1);
    show(state);

    sleep(1);

    state = light(c,0);
    show(state);
  }

  sleep(1);
  state = light(2,1);
  show(state);
  sleep(1);
  state = light(4,1);
  show(state);
  sleep(1);
  state = light(5,1);
  show(state);
  sleep(1);
}

int multipleLights(char valueC)
{
  int tmp = 0;
  int state = 0;  //no errors by default
  int value;

  if(valueC == '0')
  {
    value = 0;
  }else{
    value = 1;
  }

  for(size_t c = 1; c < LIGHTS+1; c++)
  {
    tmp = light(c, value);
    if(tmp != 1){state = -1;}
  }

  if(state == 0){
    return success;
  }

  return error;
}

int lightsOn()
{
  const char value = '1'; //
  int ret_val = multipleLights(value);
  printf("ENCENDIENDO\n");
  return ret_val;
}

int lightsOff()
{
  const char value = '0'; //
  int ret_val = multipleLights(value);
  printf("APAGANDO\n");
  return ret_val;
}

void AllLights()
{
  lightsOff();
  sleep(1);
  lightsOn();
  sleep(1);
  lightsOff();
  sleep(1);
  testlight();
}

int digitalRead(char pin)
{
  int state = 0;

  char* fullPath = setPath(pin,2);
  fp = fopen( fullPath,"r" );

  if(fp!=NULL){
    fscanf(fp, "%d", &state);
  }
  fclose(fp);

  return state;
}

/* monDoor(pin) */
int* monDoors()
{
  static int state[DOORS];
  for(size_t e = 0; e<DOORS ; e++)
  {
    state[e] = digitalRead(pinsDoors[e]);
  }
  return state;
}

/* monDoor(pin) */
int* monLights()
{
  static int state[LIGHTS];
  for(size_t e = 0; e<LIGHTS ; e++)
  {
    state[e] = digitalRead(pinsLights[e]);
  }
  return state;
}

void testmon()
{
  int* p = monLights();

  int i;

  for (i=0 ; i<LIGHTS ; i++ ) {
      printf("Estado de la led %d es: %d\n",  i+1, *(p + i) );
  }

  p = monDoors();

  for (i=0 ; i<DOORS ; i++ ) {
      printf("Estado de la puerta %d es: %d\n",  i+1, *(p + i) );
  }

}

int main(int argc, char **argv)
{
  //testinit();
  AllLights();
  testmon();
	return 0;
}
